var dataSet = [
    [ "Tiger Nixon", "System Architect", "Edinburgh", "5421", "2011/04/25", "$320,800" ],
    [ "Garrett Winters", "Accountant", "Tokyo", "8422", "2011/07/25", "$170,750" ],
    [ "Ashton Cox", "Junior Technical Author", "San Francisco", "1562", "2009/01/12", "$86,000" ],
    [ "Cedric Kelly", "Senior Javascript Developer", "Edinburgh", "6224", "2012/03/29", "$433,060" ],
    [ "Airi Satou", "Accountant", "Tokyo", "5407", "2008/11/28", "$162,700" ],
    [ "Brielle Williamson", "Integration Specialist", "New York", "4804", "2012/12/02", "$372,000" ],
    [ "Herrod Chandler", "Sales Assistant", "San Francisco", "9608", "2012/08/06", "$137,500" ],
    [ "Rhona Davidson", "Integration Specialist", "Tokyo", "6200", "2010/10/14", "$327,900" ],
    [ "Colleen Hurst", "Javascript Developer", "San Francisco", "2360", "2009/09/15", "$205,500" ],
    [ "Sonya Frost", "Software Engineer", "Edinburgh", "1667", "2008/12/13", "$103,600" ],
    [ "Jena Gaines", "Office Manager", "London", "3814", "2008/12/19", "$90,560" ],
    [ "Quinn Flynn", "Support Lead", "Edinburgh", "9497", "2013/03/03", "$342,000" ],
    [ "Charde Marshall", "Regional Director", "San Francisco", "6741", "2008/10/16", "$470,600" ],
    [ "Haley Kennedy", "Senior Marketing Designer", "London", "3597", "2012/12/18", "$313,500" ],
    [ "Tatyana Fitzpatrick", "Regional Director", "London", "1965", "2010/03/17", "$385,750" ],
    [ "Michael Silva", "Marketing Designer", "London", "1581", "2012/11/27", "$198,500" ],
    [ "Paul Byrd", "Chief Financial Officer (CFO)", "New York", "3059", "2010/06/09", "$725,000" ],
    [ "Gloria Little", "Systems Administrator", "New York", "1721", "2009/04/10", "$237,500" ],
    [ "Bradley Greer", "Software Engineer", "London", "2558", "2012/10/13", "$132,000" ],
    [ "Dai Rios", "Personnel Lead", "Edinburgh", "2290", "2012/09/26", "$217,500" ],
    [ "Jenette Caldwell", "Development Lead", "New York", "1937", "2011/09/03", "$345,000" ],
    [ "Yuri Berry", "Chief Marketing Officer (CMO)", "New York", "6154", "2009/06/25", "$675,000" ],
    [ "Caesar Vance", "Pre-Sales Support", "New York", "8330", "2011/12/12", "$106,450" ],
    [ "Doris Wilder", "Sales Assistant", "Sidney", "3023", "2010/09/20", "$85,600" ],
    [ "Angelica Ramos", "Chief Executive Officer (CEO)", "London", "5797", "2009/10/09", "$1,200,000" ],
    [ "Gavin Joyce", "Developer", "Edinburgh", "8822", "2010/12/22", "$92,575" ],
    [ "Jennifer Chang", "Regional Director", "Singapore", "9239", "2010/11/14", "$357,650" ],
    [ "Brenden Wagner", "Software Engineer", "San Francisco", "1314", "2011/06/07", "$206,850" ],
    [ "Fiona Green", "Chief Operating Officer (COO)", "San Francisco", "2947", "2010/03/11", "$850,000" ],
    [ "Shou Itou", "Regional Marketing", "Tokyo", "8899", "2011/08/14", "$163,000" ],
    [ "Michelle House", "Integration Specialist", "Sidney", "2769", "2011/06/02", "$95,400" ],
    [ "Suki Burks", "Developer", "London", "6832", "2009/10/22", "$114,500" ],
    [ "Prescott Bartlett", "Technical Author", "London", "3606", "2011/05/07", "$145,000" ],
    [ "Gavin Cortez", "Team Leader", "San Francisco", "2860", "2008/10/26", "$235,500" ],
    [ "Martena Mccray", "Post-Sales support", "Edinburgh", "8240", "2011/03/09", "$324,050" ],
    [ "Unity Butler", "Marketing Designer", "San Francisco", "5384", "2009/12/09", "$85,675" ]
];


//$(document).foundation();



$(document).ready(function(){

  document.addEventListener('dragover',function(event){
    event.preventDefault();
    return false;
  },false);

  document.addEventListener('drop',function(event){
    event.preventDefault();
    return false;
  },false);



$('#a_report').click( function() {
    $(".menu a").removeClass('active');
    $('#a_report').addClass("active");
    $(".content").removeClass('active');
    $("#panel2-1").addClass("active");
})

$('#a_doc').click( function() {
    $(".menu a").removeClass('active');
    $('#a_doc').addClass("active");
    $(".content").removeClass('active');
    $("#panel2-2").addClass("active");
})

$('#a_review').click( function() {
    $(".menu a").removeClass('active');
    $('#a_review').addClass("active");
    $(".content").removeClass('active');
    $("#panel2-3").addClass("active");
})


$('#a_msg').click( function() {
    $(".menu a").removeClass('active');
    $('#a_msg').addClass("active");
    $(".content").removeClass('active');
    $("#panel2-4").addClass("active");
})

var holder =  document.getElementById('dragdrop');
  holder.ondragover = function () {
    return false;
  };
  holder.ondragleave = holder.ondragend = function () {
    return false;
  };
  holder.ondrop = function (e) {
    e.preventDefault();
    var file = e.dataTransfer.files[0];
    console.log('File you dragged here is', file.path);
    return false;
  };

var line_plot = function (elt, data){

        var height = $(elt).parent().height()
        var width = $(elt).parent().width()
        var margin = {top: 1, right: 3,
          bottom: 3, left: 3},
        width = width - margin.left - margin.right,
        height = 50 - margin.top - margin.bottom;


        var parseDate = d3.time.format("%d-%b-%y").parse;

        var x = d3.time.scale()
            .range([0, width]);

        var y = d3.scale.linear()
            .range([height, 0]);

        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom");

        var yAxis = d3.svg.axis()
            .scale(y)
            .orient("left");

        var line = d3.svg.line()
            .x(function(d) { return x(d.date); })
            .y(function(d) { return y(d.close); });

        var svg = d3.select(elt).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)         
            .attr("viewBox","0 0 "+ width + " "+ height)
            .attr("preserveAspectRatio","xMidYMid")
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

 

          //data.forEach(function(d) {
          //  d.date = d.date;
           // d.close = +d.close;

          x.domain(d3.extent(data, function(d) { return d.date; }));
          y.domain(d3.extent(data, function(d) { return d.close; }));


          svg.append("path")
              .datum(data)
              .attr("class", "line")
              .attr("d", line);
       // });

}

var make_hist = function(elt, values ){


    // A formatter for counts.
    var formatCount = d3.format(",.0f");
    
    var height = $(elt).parent().height()
    var width = $(elt).parent().width()
    var margin = {top: 1, right: 3, bottom: 3, left: 3},
        width = width - margin.left - margin.right,
        height = 50 - margin.top - margin.bottom;

    var x = d3.scale.linear()
        .domain([0, 1])
        .range([0, width]);

    // Generate a histogram using twenty uniformly-spaced bins.
    var data = d3.layout.histogram()
        .bins(x.ticks(20))
        (values);

    var y = d3.scale.linear()
        .domain([0, d3.max(data, function(d) { return d.y; })])
        .range([height, 0]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var svg = d3.select(elt).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .attr("viewBox","0 0 "+ width + " "+ height)
        .attr("preserveAspectRatio","xMidYMid")
      .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var bar = svg.selectAll(".bar")
        .data(data)
      .enter().append("g")
        .attr("class", "bar")
        .attr("transform", function(d) { return "translate(" + x(d.x) + "," + y(d.y) + ")"; });

    bar.append("rect")
        .attr("x", 1)
        .attr("width", x(data[0].dx) - 1)
        .attr("height", function(d) { return height - y(d.y); });

}
//make_hist('body')
//document.make_hist = make_hist
var generate_hist = function(elt){
    var values = d3.range(1000).map(d3.random.bates(10));
   
    make_hist(elt, values)
  }
 var generate_line= function(elt){
    var end_ts = new Date().getTime();
    var range = 60000;

    var start_ts = end_ts - range;
    var n = 30,
    random = d3.random.normal(0, .2),
    data = d3.range(n).map(function(i) {
      return { date:new Date(end_ts - (n*1000) + i*1000),       close:random() };
    });

    line_plot(elt, data)
    

}

var chart_update = function(elt){
  var chart = $(elt);
  console.log(chart.parent().width() )
  chart.attr("width", chart.parent().width())

}
$(window).on("resize", function() {
    console.log("resize")
    chart_update("#chart1")
    chart_update("#chart2")
})



$(document).foundation();

  var hists = $('[id*=hist]')//.forEach(item, fucntio)
  
  $.each(hists, function(e,v){
     
      generate_hist("#"+v.id)
  })

  var lines = $('[id*=line]')

  $.each(lines, function(e,v){
      generate_line("#"+v.id)
  })

   $('#tttable').DataTable( {
        data: dataSet,
        searching: false,
        ordering:  false,
        paging: false,
        columns: [
            { title: "Name" },
            { title: "Position" },
            { title: "Office" },
            { title: "Extn." },
            { title: "Start date" },
            { title: "Salary" }
        ]
    } );


})
